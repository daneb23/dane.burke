# About me
 I am a Northern Ohio native. Born and raised in Huron county in a small town on the Vermilion River.

![This is me with parents on their 50th Anniversary in Aug 2014](../images/wFam.jpg)

 My Dad passed in June of this year and is greatly missed all the moments of each day. 

## My interests
- My 2 dogs: HoneyBun and Bear 
![](../imageswHB-Bear.jpg)
![This is Bear. She is the mama. She is half Shi-Tzu and half Feist. ](../images/wBear.jpg)
![This is HoneyBun aka Bun. She is the Bear's daughter. She is half "Bear" and half Pug. ](../images/wBun.jpg)
- My newly acquired cat: RavenPaw
![Raven. He is full of fire](../images/wRaven.jpg)
- Making, building, creating, tinkering, inventing, designing
- Skiing
- Riding Motorcycles
- RVs
- Traveling

# LCCC's Digital Fabrication Program

Lorain County Community College is located in Elyria, OH. They have great technology programs and facilities. When I decided to go back to school, I was interested in their automation program. But then I discovered the FabLab

## FabLab
[FabLab](https://www.lorainccc.edu/campana/fab-lab)

You will definitely want to check out the site if you're not already familiar with it. Everyting you need to know can be found there so I want go into details.  I'll just say that a number of years ago I stumbled upon a company that started out in California called TechShop.  It was a membership based business that had every imaginable machine to make.  With a membership and safety training, you could use any machine in the facility. They ended up spreading and had 10 locations across the country (with Detroit being the closest)  I visited a couple of them and was super excited about the opportunity to take part. Unfortunately for me, I didn't live close enough to make the reasonable, but not so cheap, membership cost worth it.  Unfortunetly for TechShop (and imo, the world) they closed without warning in 2017. I don't know the details, but my guess is that memberships didn't sell as they had hoped and they couldn't defray the exorbitant cost of the multi-million dollar equipment they owned and maintained. So, even though FabLab is a miniature version of what TechShop was, I was super excited to learn there was one in our back yard.  So it was a no brainer for me to go for the Associate's degree in Digital Fabrication. 

[Wikipedia on TechShop](https://en.wikipedia.org/wiki/TechShop)
 


 



![This is Bear](../images/20230113_153049506_iOS.jpg)

